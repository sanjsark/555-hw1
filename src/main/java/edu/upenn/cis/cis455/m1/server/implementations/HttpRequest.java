package edu.upenn.cis.cis455.m1.server.implementations;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.util.HttpParsing;

public class HttpRequest extends Request {
    
    final static Logger logger = LogManager.getLogger(HttpParsing.class);
    
    private int port;
    private String host;
    
    private String uri;
    
    private Map<String, String> headers;
    private Map<String, List<String>> parms;
    
    public HttpRequest(Socket socket) {
        try {
            InputStream inputStream = socket.getInputStream();
            port = socket.getLocalPort();
            host = socket.getLocalAddress().toString();

            headers = new HashMap<String, String>();
            parms = new HashMap<String, List<String>>();
            
            String remoteIp = socket.getRemoteSocketAddress().toString();
            uri = HttpParsing.parseRequest(remoteIp, inputStream, headers, parms);
        } catch (IOException e) {
            logger.error(e);
        }    
    }

	@Override
	public String requestMethod() {
		return headers.get("Method");
	}

	@Override
	public String host() {
		return headers.get("host") == null ? host : headers.get("host");
	}

	@Override
	public String userAgent() {
		return headers.get("user-agent");
	}

	@Override
	public int port() {
		return port;
	}

	@Override
	public String pathInfo() {
		return uri;
	}

	@Override
	public String url() {
	    //TODO
		return null;
	}

	@Override
	public String uri() {
	    //TODO
		return null;
	}

	@Override
	public String protocol() {
		return headers.get("protocolVersion");
	}

	@Override
	public String contentType() {
		return headers.get("Content-Type");
	}

	@Override
	public String ip() {
		return headers.get("http-client-ip");
	}

	@Override
	public String body() {
	    // TODO
		return null;
	}

	@Override
	public int contentLength() {
	    // TODO
		return Integer.parseInt(headers.get("Content-Length"));
	}

	@Override
	public String headers(String name) {
		return headers.get(name);
	}

	@Override
	public Set<String> headers() {
		return headers.keySet();
	}
}
