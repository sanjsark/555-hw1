package edu.upenn.cis.cis455.m1.server.implementations;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import edu.upenn.cis.cis455.m1.server.interfaces.Response;

public class HttpResponse extends Response {

    private Map<String, String> headers;
    private String contentType;
    private int contentLength;

	@Override
	public String getHeaders() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> e : headers.entrySet()) {
            sb.append(e.getKey() + ": " + e.getValue() + "\r\n");
        }
        
        ZonedDateTime currTime = ZonedDateTime.now();
        String date = currTime.format(DateTimeFormatter.RFC_1123_DATE_TIME);
        sb.append("Date: " + date + "\r\n");
        
        sb.append("Content-Type: " + contentType + "\r\n");
        sb.append("Content-Length: " + contentLength + "\r\n");
        sb.append("Connection: close\r");
        return sb.toString();
	}
	
	public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
	} 
	
	public void setContentType(String ct) {
	    contentType = ct;
	}
	
	public void setContentLength(int cl) {
	    contentLength = cl;
	}
}
