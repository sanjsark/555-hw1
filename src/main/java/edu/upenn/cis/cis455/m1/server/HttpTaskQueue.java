package edu.upenn.cis.cis455.m1.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
    
    static final Logger logger = LogManager.getLogger(HttpTaskQueue.class);
    
    List<HttpTask> q; // shared queue
    int maxSize; // max size of the queue
    
    public HttpTaskQueue(int maxSize) {
        this.maxSize = maxSize;
        q = new ArrayList<HttpTask>();
    }
    
    public void add(HttpTask t) {
        // Wait until the queue has space
        while (q.size() == maxSize) {
            synchronized (q) {
                try {
					q.wait();
				} catch (InterruptedException e) {
					logger.error(e);
				}
            }
        }
        // Add to q
        synchronized (q) {
            q.add(t);
            q.notify();
        }
    }
    
    public HttpTask poll() {
        // Wati if q is empty
        while (q.isEmpty()) {
            synchronized (q) {
                try {
					q.wait();
				} catch (InterruptedException e) {
					logger.error(e);
				}
            }
        }
        // Remove from q
        synchronized (q) {
            HttpTask res = q.remove(0);
            q.notify();
            return res;
        }
        
    }
}
